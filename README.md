# ts+react + react-router + mobx + babylon 模板项目

## 项目初始化安装依赖库
```
npm install
yarn
```

### 项目热重载调试启动
```
npm run start
yarn start
```

### 项目打包
```
npm run build
yarn build
```

### 项目测试 
```
npm run test
yarn test
```

### 分析打包大小 
```
npm run analyze
yarn analyze
```

### 检查代码风格 （本项目自动检测不需要额外运行）
```
npm run lint
```

### react-cli详细配置
官网 [Configuration Reference](https://www.html.cn/create-react-app/).

### 项目结构汇总
```
│  .browserslistrc --- 浏览器白名单（做浏览器兼容配置,现在可以直接在package.json配置）
│  .gitignore --- git忽略提交配置
│  postcss.config.js --- css代码转换工具配置（可以自动兼容浏览器版本或者利用插件进行样式单位转换，配置现在放在package.json配置）
│  setupProxy.js --- react代理服务器配置 http-proxy-middleware
│  package.json --- node npm模块及指令配置文件
│  README.md --- 说明文档
├─src --- 项目代码主工作目录
│   │  App.test.tsx --- 根节点组件页面
│   │  index.tsx  --- 入口文件
│   ├─asset --- 静态资源
│   ├─components  --- 常用组件、公用组件
│   ├─types --- 全局类型声明文件
│   ├─models --- 数据模型类 （表示数据格式及请求地址）
│   ├─views --- 页面文件目录
│   ├─utils --- 常用工具 http等
│   ├─stores --- 前端数据状态管理器
│   └─game3d --- babylon game3d项目目录
│       ├─guis --- babylon gui
|       ├─resourcesManage --- babylon 资源管理
|       └─scenes --- babylon 场景
├─public  --- 公共资源
│   ├─font --- 字体
│   ├─img --- 图片
│   ├─svg --- svg
│   ├─glsl --- glsl 着色器文件
│   ├─mesh --- 模型文件
└─node_modules  --- 安装好的npm模块

```
### 项目主要库及工具

- react :react框架
- create-react-app :react框架cli
- react-router :react路由
- mobx :mobx数据状态管理
- typescript :typescript js超级集合面向对象写法
- storybook :ui组件分离式开发同时提供展示及文档方法
- analyze :分析打包大小问题
- antd :蚂蚁金服ui组件库
- axios :请求库
- json2typescript :json数据转ts类对象处理库
- lodash :数据处理工具
- moment :时间处理库
- babylonjs :游戏级3d引擎
- cannon :js物理引擎库