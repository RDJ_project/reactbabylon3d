import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./App.css";
import Home from "./view/Home";
import Game from "./view/Game";
import City3d from "./view/City3d";

function App() {
  return (
    <div className="App">
      <Router>
        <div>
          <nav>
            <div className="link">
              <p>
                <Link to="/">City3d</Link>
              </p>
              <p>
                <Link to="/home">Home</Link>
              </p>
              <p>
                <Link to="/game?name=rdj">game</Link>
              </p>
            </div>
          </nav>

          <Switch>
            <Route path="/" component={City3d}></Route>
            <Route path="/game" component={Game}></Route>
            <Route path="/home" component={Home}></Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
