// js 运行环境判断
export default {
  isWeiXing() {
    // 是否是微信浏览器内

    let ua = window.navigator.userAgent.toLowerCase();
    // tslint:disable-next-line:max-line-length
    // mozilla/5.0 (iphone; cpu iphone os 9_1 like mac os x) applewebkit/601.1.46 (khtml, like gecko)version/9.0 mobile/13b143 safari/601.1
    let result = ua.match(/MicroMessenger/i);

    if (result && result.indexOf("micromessenger")) {
      return true;
    } else {
      return false;
    }
  },
};
