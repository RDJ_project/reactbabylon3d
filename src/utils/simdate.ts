import moment from 'moment' // 时间处理库(建议后台做时间处理)
// console.log(moment().days(-1).format('Y-M-D'))



export default {

    YMD(date: moment.MomentInput) {
        // 日期转化为YMD模式(2017/07/03-11:09:50)
        return moment(date).format('Y-M-D')
    },

    YM(date: moment.MomentInput) {
        // 日期转化为YMD模式(2017/07/03-11:09:50)
        return moment(date).format('Y-M')
    },
    SJCtoTime(date: number) {
        // 时间戳转默认时间格式
        // console.log(date)
        if ( date != null && typeof(date) !== "undefined") {
            // console.info('时间戳', date)
            return moment.unix(date).format('Y-M-D H:mm:ss')
        } else {
            return ''
        }
    },
    TimetoSJC(date: moment.MomentInput) {
        // 时间转时间戳
        if (date !== '' && date != null && typeof(date) !== "undefined") {
            return moment(date).format('X')
        } else {
            return ''
        }
    },
    TMDTime(date: moment.MomentInput) {
        // 热力图时间格式

        return moment(date).format('Y-M-D H:mm:ss')
    },
    todayTime() {
        // 今天
        return moment().format('Y-M-D H:mm')

    },

    min5_ago() {
        // 5分钟前
        return moment().subtract('minutes', 5).format('Y-M-D H:mm:ss')
    },
    min10_ago() {
        // 10分钟前
        return moment().subtract('minutes', 10).format('Y-M-D H:mm:ss')
    },
    min30_ago() {
        // 30分钟前
        return moment().subtract('minutes', 30).format('Y-M-D H:mm:ss')
    },
    hour1_ago() {
        // 1小时前
        return moment().subtract('hours', 1).format('Y-M-D H:mm:ss')
    },
    hour6_ago() {
        // 6小时前
        return moment().subtract('hours', 6).format('Y-M-D H:mm:ss')
    },
    hour12_ago() {
        // 12小时前
        return moment().subtract('hours', 12).format('Y-M-D H:mm:ss')
    },
    day1_ago() {
        // 1天前
        return moment().subtract('days', 1).format('Y-M-D H:mm:ss')
    },
    day7_ago() {
        // 7天前
        return moment().subtract('days', 6).format('Y-M-D H:mm:ss')
    },
    day15_ago() {
        // 15天前
        return moment().subtract('days', 14).format('Y-M-D H:mm:ss')
    },
    today() {
        // 今天
        return moment().format('Y-M-D H:mm:ss')
    },
    yesterday() {
        // 昨天
        return moment().subtract('days', 1).format('Y-M-D H:mm:ss')
    },
    aweek() {
        // 一周内
        return {
            start: moment().days(-7).format('Y-M-D'),
            end: moment().format('Y-M-D')
        }
    },
    amounth() {
        // 一月内
        return {
            start: moment().days(-30).format('Y-M-D'),
            end: moment().format('Y-M-D')
        }
    }

}