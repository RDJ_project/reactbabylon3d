// 微信前端授权
// import axios from "axios"; // 请求库
import myHttp from "./myHttp";
import environment from "./environment"; // 环境判断
import { WxAuthorResModel, WxConfigModel } from "../models/common/wx";
const wx = require("weixin-js-sdk");
const shareimg = require("../img/logo.jpg"); // 分享图片

export default {
  getcode(
    appid: string,
    redirect_uri: string = "http://wzr.wztz.org.cn",
    response_type: string = "code",
    scope: string = "snsapi_userinfo",
    wechat_redirect: string = "http://wzr.wztz.org.cn"
  ) {
    redirect_uri = encodeURIComponent(redirect_uri);
    wechat_redirect = encodeURIComponent(wechat_redirect);
    // 微信前端授权 获取code令牌(需要跳转后) 然后传给后端获取用户资料
    //     appid	是	公众号的唯一标识
    // redirect_uri	是	授权后重定向的回调链接地址， 请使用 urlEncode 对链接进行处理
    // response_type	是	返回类型，请填写code
    // tslint:disable-next-line:max-line-length
    // scope	是	应用授权作用域，snsapi_base （不弹出授权页面，直接跳转，只能获取用户openid），snsapi_userinfo （弹出授权页面，可通过openid拿到昵称、性别、所在地。并且， 即使在未关注的情况下，只要用户授权，也能获取其信息 ）
    // state	否	重定向后会带上state参数，开发者可以填写a-zA-Z0-9的参数值，最多128字节
    // #wechat_redirect	是	无论直接打开还是做页面302重定向时候，必须带此参数
    if (environment.isWeiXing()) {
      window.location.href =
        "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" +
        appid +
        "&redirect_uri=" +
        redirect_uri +
        "&response_type=" +
        response_type +
        "&scope=" +
        scope +
        "&wechat_redirect=" +
        wechat_redirect;
    }
  },
  getWXtoken_openid(code: string) {
    // 把之前拿到的 code 传给后端
    // 该接口获取的是网页授权 access_token 以及openid
    //         appid	是	公众号的唯一标识
    // secret	是	公众号的appsecret
    // code	是	填写第一步获取的code参数
    // grant_type	是	填写为authorization_code
    // console.info('获取access_token')

    return new Promise((resolve, reject) => {
      if (environment.isWeiXing()) {
        // let url='https://api.weixin.qq.com/sns/oauth2/access_token' //微信官方地址 但是前端会跨域要后台对接
        let url = "/wzr/wxUser/getUserInfoAccessToken";

        // axios.get(url, {
        //     params: {
        //         appid: appid,
        //         secret: secret,
        //         code: code,
        //         grant_type: grant_type
        //     }
        // }).then(
        //     res => {
        //         resolve(res)
        //         console.info('授权,用户信息', res)

        //     }
        // ).catch(
        //     err => {
        //         console.info('openid异常', res)
        //         reject(err)

        //     }
        // )

        myHttp
          .post<WxAuthorResModel>(url, WxAuthorResModel, {
            code,
          })
          .then((res) => {
            resolve(res);
            // console.info('授权,用户信息', res)
          })
          .catch((err) => {
            // console.info('openid异常', res)
            reject(err);
          });
      } else {
        return resolve(false);
      }
    });
  },
  wxSdkInit(url: string) {
    // 微信 jssdk初始化 签名加配置
    let currenUrl = encodeURIComponent(window.location.href.split("#")[0]); // 当前页面地址

    // 微信环境的初始化（原生版）
    return new Promise((resolve, reject) => {
      if (environment.isWeiXing()) {
        myHttp
          .post<WxConfigModel>(url, WxConfigModel, {
            url: currenUrl,
          })
          .then((res) => {
            let wxoption = res.data; // wx jssdk配置
            console.info("微信配置", wxoption);
            wx.config({
              debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
              appId: wxoption.appId || "", // 必填，公众号的唯一标识
              timestamp: wxoption.timestamp || "", // 必填，生成签名的时间戳
              nonceStr: wxoption.nonceStr || "", // 必填，生成签名的随机串
              secret: wxoption.secret || "", // 公众号的appsecret
              signature: wxoption.signature || "", // 必填，签名
              jsApiList:
                wxoption.jsApiList.length > 0
                  ? wxoption.jsApiList
                  : [
                      "updateTimelineShareData",
                      "updateAppMessageShareData",
                      "onMenuShareTimeline",
                      "onMenuShareAppMessage",
                      "onMenuShareQQ",
                      "onMenuShareWeibo",
                      "onMenuShareQZone",
                      // 'startRecord',
                      // 'stopRecord',
                      // 'onVoiceRecordEnd',
                      // 'playVoice',
                      // 'pauseVoice',
                      // 'stopVoice',
                      // 'onVoicePlayEnd',
                      // 'uploadVoice',
                      // 'downloadVoice',
                      // 'chooseImage',
                      // 'previewImage',
                      // 'uploadImage',
                      // 'downloadImage',
                      // 'translateVoice',
                      // 'getNetworkType',
                      // 'openLocation',
                      // 'getLocation',
                      // 'hideOptionMenu',
                      // 'showOptionMenu',
                      // 'hideMenuItems',
                      // 'showMenuItems',
                      // 'hideAllNonBaseMenuItem',
                      // 'showAllNonBaseMenuItem',
                      // 'closeWindow',
                      // 'scanQRCode',
                      "chooseWXPay",
                      // 'openProductSpecificView',
                      // 'addCard',
                      // 'chooseCard',
                      // 'openCard'
                    ], // 必填，需要使用的JS接口列表
            }); // 微信 sdk配置

            wx.ready(() => {
              // 微信sdk准备完成
              console.info("微信", wx);
              resolve(wx);
            });
          })
          .catch((err) => {
            reject(err);
          });
      } else {
        resolve(false);
      }
    });
  },
  wxPay(
    timeStamp: number,
    nonceStr: string,
    packages: any,
    signType: string,
    paySign: string,
    appId: any
  ) {
    // 微信支付
    return new Promise((resolve, reject) => {
      if (environment.isWeiXing()) {
        wx.chooseWXPay({
          // 微信支付（jssdk版）
          timestamp: 0, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
          nonceStr: "", // 支付签名随机串，不长于 32 位
          package: "", // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=\*\*\*）
          signType: "", // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
          paySign: "", // 支付签名
          success(res: any) {
            // 支付成功后的回调函数
            console.log(res);
          },
        });

        WeixinJSBridge.invoke(
          // 微信支付（内置版）
          "getBrandWCPayRequest",
          {
            appId: "wx2421b1c4370ec43b", // 公众号名称，由商户传入
            timeStamp: "1395712654", // 时间戳，自1970年以来的秒数
            nonceStr: "e61463f8efa94090b1f366cccfbbb444", // 随机串
            package: "prepay_id=u802345jgfjsdfgsdg888",
            signType: "MD5", // 微信签名方式：
            paySign: "70EA570631E4BB79628FBCA90534C63FF7FADD89", // 微信签名
          },
          (res: any) => {
            if (res.err_msg === "get_brand_wcpay_request:ok") {
              // 使用以上方式判断前端返回,微信团队郑重提示：
              // res.err_msg将在用户支付成功后返回ok，但并不保证它绝对可靠。
            }
          }
        );
      }
    });
  },
  configWxShare(
    wx: any, // 微信对象（必须）
    title = document.title || "", // 分享标题（可选）
    desc = "在这里遇见世界温州人", // 分享描述（可选）
    link = window.location.href || "", // 分享链接（可选）
    imgUrl = "http://wzr.wztz.org.cn/static/img/logo.jpg" // 分享图片（可选）
  ) {
    // 配置微信分享
    if (environment.isWeiXing() && wx) {
      console.info("微信", desc);

      const success = () => {
        // 分享确认
        // console.log('分享成功')
        alert("分享成功");
      };

      const cancel = () => {
        // 分享取消
        // console.log('分享取消')
        alert("分享取消");
      };

      // 1.4.0 新分享接口
      // wx.updateAppMessageShareData({ //自定义“分享给朋友”及“分享到QQ”按钮的分享内容
      //     title: title, // 分享标题
      //     desc: desc, // 分享描述
      //     link: link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
      //     imgUrl: imgUrl, // 分享图标
      //     // success: success
      // })

      // wx.updateTimelineShareData({ //自定义“分享到朋友圈”及“分享到QQ空间”按钮的分享内容
      //         title: title, // 分享标题
      //         link: link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
      //         imgUrl: imgUrl, // 分享图标
      //         // success: success
      // })
      // -----------------
      // 分享微博
      wx.onMenuShareWeibo({
        // 获取“分享到腾讯微博”按钮点击状态及自定义分享内容接口
        title, // 分享标题
        desc, // 分享描述
        link, // 分享链接
        imgUrl, // 分享图标
        success,
        cancel,
      });
      // ---------------
      // 旧分享接口
      wx.onMenuShareTimeline({
        // 获取“分享到朋友圈”按钮点击状态及自定义分享内容接口
        title, // 分享标题
        link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
        imgUrl, // 分享图标
        success,
      });
      wx.onMenuShareAppMessage({
        // 获取“分享给朋友”按钮点击状态及自定义分享内容接口
        title, // 分享标题
        desc, // 分享描述
        link, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
        imgUrl, // 分享图标
        type: "link", // 分享类型,music、video或link，不填默认为link
        // dataUrl: '', // 如果type是music或video，则要提供数据链接，默认为空
        success,
      });
      wx.onMenuShareQQ({
        // 获取“分享到QQ”按钮点击状态及自定义分享内容接口
        title, // 分享标题
        desc, // 分享描述
        link, // 分享链接
        imgUrl, // 分享图标
        success,
        cancel,
      });

      wx.onMenuShareQZone({
        // 获取“分享到QQ空间”按钮点击状态及自定义分享内容接口
        title, // 分享标题
        desc, // 分享描述
        link, // 分享链接
        imgUrl, // 分享图标
        success,
        cancel,
      });
    }
  },
};
