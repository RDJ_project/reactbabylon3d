import React, { Component } from "react";
import styles from "./main.module.css";
import { inject, observer } from "mobx-react";

import { AppStore } from "../../stores/App.store";

export interface checkState {
  index: number;
  msg: string;
}
const defaultProps = {
  //设置props默认值方法
  index: 0,
};
export type checkProps = {
  index: number;
  appStore?: AppStore;
} 

@inject("appStore")
@observer
class Check extends Component<checkProps, checkState> {
  public static defaultProps:checkProps = defaultProps;
  constructor(props: checkProps) {
    super(props);
  }

  render() {
    const { todos } = this.props.appStore as AppStore;
    let todosDiv = todos.map((item, index) => {
      return <CheckTodo index={index} />;
    });
    return <div className="App">{todosDiv}</div>;
  }
}

@inject("appStore")
@observer
class CheckTodo extends Component<checkProps, checkState> {
  public static defaultProps:checkProps = defaultProps;
  constructor(props: checkProps) {
    super(props);
  }

   handleClick() {
    const appStore = this.props.appStore as AppStore;
    console.log('props',this);
    
    let index = this.props.index;
    appStore.handleClick(index);
  }

  render() {
    const appStore = this.props.appStore as AppStore;
    let index = this.props.index;
    let todo = appStore.todos[index];
    return (
      <p>
        <input
          type={"checkbox"}
          checked={todo.checked}
          onClick={this.handleClick.bind(this)}
        />
        {todo.text}:{index}
      </p>
    );
  }
}

export default Check;
