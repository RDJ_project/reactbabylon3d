import React from "react";
import styles from "./main.module.css";
import Game from "../../game3d/scenes";
export interface game3dState {
  msg: string;
}

export interface game3dProps {
  show?: boolean; //是否显示
}

export default class Game3d extends React.Component<game3dProps, game3dState> {
  state = { msg: "game3d" };
  private gameCanvas: React.RefObject<HTMLCanvasElement>;
  static defaultProps = {
    //设置props默认值方法
    show: true,
  };
  constructor(props: game3dProps) {
    super(props);
    this.gameCanvas = React.createRef();
  }

  render() {
    return (
      <canvas
        className={styles.Game3d}
        ref={this.gameCanvas}
        width=""
        height=""
      ></canvas>
    );
  }
  componentDidMount() {
    let gameCanvas = this.gameCanvas.current;
    if (gameCanvas) {
      gameCanvas.setAttribute("width", `${window.innerWidth}`);
      gameCanvas.setAttribute("height", `${window.innerHeight}`);
      window.addEventListener("resize", () => {
        //窗口调整大小
        if (gameCanvas) {
          gameCanvas.setAttribute("width", `${window.innerWidth}`);
          gameCanvas.setAttribute("height", `${window.innerHeight}`);
        }
      });
      let myGame = new Game(gameCanvas);
      myGame.createScene();
      myGame.doRender();
    }
  }
}
