
import {
    JsonObject,
    JsonProperty
} from "json2typescript"; // json转ts对象库
export interface ResponseData<T = any> {
    // 服务器端返回数据模接口

    code: number // 业务请求返回码
    mes: string; // 信息
    status: string; // 状态
    data: T  // 数据
}
@JsonObject(ResponseModel)
export class ResponseModel {
    // 服务器端返回数据模型
    @JsonProperty("code", Number)
    code: number = 0; // 业务请求返回码
    @JsonProperty("mes", String)
    mes: string = ""; // 信息
    @JsonProperty("status", String)
    status: string = ""; // 状态
    @JsonProperty("data")
    data: any = undefined; // 数据
}