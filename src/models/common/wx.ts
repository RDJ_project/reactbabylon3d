
import { JsonObject, JsonProperty } from "json2typescript";
@JsonObject(WxAuthorResModel)
export class WxAuthorResModel {
  @JsonProperty('openid', String)
  openid: string = "" // openid
}

@JsonObject(WxConfigModel)
export class WxConfigModel {
  @JsonProperty('debug', Boolean)
  debug: boolean = false // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
  @JsonProperty('appId', String)
  appId: string = "" // 必填，公众号的唯一标识
  @JsonProperty('timestamp', String)
  timestamp: string = ""// 必填，生成签名的时间戳
  @JsonProperty('nonceStr', String)
  nonceStr: string = "" // 必填，生成签名的随机串
  @JsonProperty('secret', String)
  secret: string = "" // 公众号的appsecret
  @JsonProperty('signature', String)
  signature: string = "" // 必填，签名
  @JsonProperty('jsApiList', [String])
  jsApiList: string[] = [] // 必填，需要使用的JS接口列表
}

export const urls = {
  HQYHSJ: ''
}
