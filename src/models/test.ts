import { JsonObject, JsonProperty } from "json2typescript";
/**
 * 测试模型
 *
 * @export
 * @class MyTestModel
 */
@JsonObject(MyTestModel)
export default class MyTestModel {
  @JsonProperty("mes", String)
  mes: string = ""; // 信息
}

export const urls = {
  HQCSSJ: '/test'
}
