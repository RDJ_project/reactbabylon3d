import { JsonObject, JsonProperty } from "json2typescript";
/**
 * 用户信息数据模型
 *
 * @export
 * @class UserInfoModel
 */
@JsonObject(UserInfoModel)
export default class UserInfoModel {
  @JsonProperty("area", String)
  area: string = ""; // 地区
  @JsonProperty("birthdate", String)
  birthdate = ""; // 生日
  @JsonProperty("creatime", String)
  creatime = ""; // 创建时间
  @JsonProperty("creator", String)
  creator = ""; // 创建者
  @JsonProperty("icon", String)
  icon = ""; // 用户图标
  @JsonProperty("idc", String)
  idc = ""; // 身份证号
  @JsonProperty("mail", String)
  mail = ""; // 邮箱
  @JsonProperty("phoneNumber", String)
  phoneNumber = ""; // 电话号码
  @JsonProperty("sex", Number)
  sex = 0; // 性别
  @JsonProperty("status", Number)
  status = 1; //
  @JsonProperty("updateTime", String)
  updateTime = ""; // 更新时间
  @JsonProperty("updatetor", String)
  updatetor = ""; // 更新人
  @JsonProperty("wxId", String)
  wxId = ""; // 微信账号id
  @JsonProperty("wxName", String)
  wxName = ""; // 微信账号
  @JsonProperty("wxType", String)
  wxType = ""; //
  @JsonProperty("wxUserName", String)
  wxUserName = ""; // 姓名
}

export const urls = {
  HQYHSJ: ''
}
