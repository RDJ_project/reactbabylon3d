import { action, observable } from "mobx";

 export class AppStore {
  @observable todos: { checked: boolean; text: string }[];

  constructor() {
    this.todos = [
      { checked: false, text: "hello" },
      { checked: true, text: "world" },
    ];
  }

  @action.bound handleClick(index: number) {
    let todos = this.todos;
    todos[index].checked = !todos[index].checked;
  }
}
export default new AppStore();
