const { createProxyMiddleware } = require('http-proxy-middleware'); // 代理服务中间件
module.exports = function (app) {
    // 代理服务器地址
    app.use(createProxyMiddleware('/api', {
        target: 'http://localhost:8090',
        secure: false,
        changeOrigin: true,
        pathRewrite: {
            "^/api": ""
        },
    }));
 
};