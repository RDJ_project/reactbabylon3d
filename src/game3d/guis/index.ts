import * as BABYLON from "@babylonjs/core/Legacy/legacy";
import * as GUI from "@babylonjs/gui";
import MyScenes from "../../game3d/scenes";

export default class myUi {
  _myScene: MyScenes; //我的场景
  _gui: GUI.AdvancedDynamicTexture; //全屏ui
  mySign: GUI.Control[] | undefined; //我的显示标签ui包含的ui组
  myCanvas: HTMLCanvasElement;

  constructor(myScene: MyScenes) {
    this._myScene = myScene;
    this._gui = GUI.AdvancedDynamicTexture.CreateFullscreenUI(
      "UI",
      true,
      this._myScene._scene
    );
    this._gui.renderScale = 1;
    console.info("场景", this._myScene);
    this.myCanvas = myScene._canvas;
  }

  showSign(text: string, mesh: BABYLON.Mesh) {
    // 显示标签信息
    // text显示文本 mesh模型

    let mySign: GUI.Control[] = []; //我的显示标签ui包含的ui组
    console.info("signText", text);

    // this._gui.idealWidth = 600;

    let rect1 = new GUI.Rectangle();
    rect1.width = "100px";
    rect1.height = "40px";
    // rect1.paddingTop = "10px"
    rect1.cornerRadius = 20;
    rect1.color = "Orange";
    rect1.thickness = 4;
    rect1.background = "green";
    rect1.alpha = 0;
    this._gui.addControl(rect1);
    rect1.linkWithMesh(mesh);
    rect1.linkOffsetX = 180;
    rect1.linkOffsetY = -70;
    mySign.push(rect1);

    let label = new GUI.TextBlock();
    label.text = text;
    // label.width = 100
    // label.height = 40
    // label.top = 23;
    // label.paddingTop = -20;
    label.textHorizontalAlignment = GUI.Control.HORIZONTAL_ALIGNMENT_LEFT;
    label.alpha = 0;
    rect1.addControl(label);
    mySign.push(label);

    let target = new GUI.Ellipse();
    target.width = "10px";
    target.height = "10px";
    target.color = "Orange";
    target.thickness = 4;
    target.background = "green";
    target.alpha = 0;
    this._gui.addControl(target);
    target.linkWithMesh(mesh);
    mySign.push(target);

    let line1 = new GUI.Line();
    line1.lineWidth = 3;
    line1.color = "Orange";
    line1.y2 = 20;
    line1.linkOffsetX = 90;
    line1.alpha = 0;
    this._gui.addControl(line1);
    line1.linkWithMesh(mesh);
    line1.connectedControl = rect1;
    mySign.push(line1);

    let line2 = new GUI.Line();
    line2.lineWidth = 4;
    line2.color = "Orange";
    line2.x2 = -40;
    line2.y2 = 25;
    line2.linkOffsetX = 4;
    line2.alpha = 0;
    this._gui.addControl(line2);
    line2.linkWithMesh(mesh);
    line2.connectedControl = line1;
    mySign.push(line2);
    setTimeout(() => {
      mySign.forEach((item) => {
        item.alpha = 1;
      });
    }, 100);

    return mySign;
  }
  hideSign(sign: GUI.Control[]) {
    // 隐藏标签信息
    /*----显示弹窗----*/
    sign.forEach((item: GUI.Control) => {
      this._gui.removeControl(item);
    });
  }
}
