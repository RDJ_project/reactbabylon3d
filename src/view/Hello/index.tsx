import React from "react";
import styles from "./main.module.css"
export interface helloState {
  msg: string;
}

export interface helloProps {
  name?: string;
}

export default class Hello extends React.Component<helloProps, helloState> {
  state = { msg: "hello hello" };
  static defaultProps = { //设置props默认值方法
    name:"rdj"
  }
  constructor(props: helloProps) {
    super(props);
  }

  render() {
    return (
      <div className={styles.hello}>
        {this.state.msg} {this.props.name}
      </div>
    );
  }
}
