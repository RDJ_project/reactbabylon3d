import React from "react";
import logo from "../../logo.svg";
import Hello from "../Hello";
import styles from "./main.module.css";
import { Switch, Route, useRouteMatch, Router } from "react-router-dom";

export interface homeState {
  msg: string;
}

export interface homeProps {
  name?: string;
}

export default class Home extends React.Component<homeProps, homeState> {
  state = { msg: "hello home" };
  static defaultProps = {
    //设置props默认值方法
    name: "rdj",
  };
  constructor(props: homeProps) {
    super(props);
  }

  render() {
    // let match = useRouteMatch();
    // console.log(match);

    return (
      <div>
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <p>
            Edit <code>src/App.tsx</code> and save to reload.
          </p>
          <a
            className="App-link"
            href="https://reactjs.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            Learn React
          </a>
        </header>
        <div className={styles.home}>
          <Switch>
            <Route path="/" component={Hello}>
           
            </Route>
          </Switch>
        </div>
      </div>
    );
  }
}
