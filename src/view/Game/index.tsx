import React from "react";
// import {History,Location} from "@types"
import styles from "./main.module.css";
import {
  // useHistory,
  // useLocation,
  // useParams,
  Link,
  RouteComponentProps,
} from "react-router-dom";
import Check from "../../components/Check"

export interface gameState {
  msg: string;
}

export interface gameProps extends RouteComponentProps { //继承路由prop参数 获取路由信息
  name?: string;
}

export default class Game extends React.Component<gameProps, gameState> {
  state = { msg: "game game" };
  static defaultProps = {
    //设置props默认值方法
    name: "游戏demo",
  };
  constructor(props: gameProps) {
    super(props);
  }

  render() {
    // let history = useHistory(); //函数组件才能用
    // let location = useLocation(); //函数组件才能用
    // let params = useParams(); //函数组件才能用
    console.log(this);

    return (
      <div className={styles.game}>
        {this.props.name}
        {this.props.location.search}
        <Link to="/">home</Link>
        <Link to="/game">game</Link>
        <Check ></Check>
      </div>
    );
  }
}
