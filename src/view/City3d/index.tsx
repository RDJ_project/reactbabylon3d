import React from "react";
// import {History,Location} from "@types"
import styles from "./main.module.css";
import { RouteComponentProps } from "react-router-dom";
import Game3d from "../../components/Game3d";

export interface city3dState {
  msg: string;
}

export interface city3dProps extends RouteComponentProps {
  //继承路由prop参数 获取路由信息
  name?: string;
}

export default class City3d extends React.Component<city3dProps, city3dState> {
  state = { msg: "city3d" };
  static defaultProps = {
    //设置props默认值方法
    name: "游戏demo",
  };
  constructor(props: city3dProps) {
    super(props);
  }

  render() {
    return (
      <div className={styles.city3d}>
        <Game3d></Game3d>
      </div>
    );
  }
}
